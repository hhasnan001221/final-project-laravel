<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    use HasFactory;

    protected $table = 'films';

    protected $primaryKey = 'id';

	protected $fillable = ['judul', 'ringkasan', 'tahun', 'poster', 'genre_id'];

    public function Genre()
    {
        return $this->belongsTo(Genre::class, 'id');
    }

    public function Peran()
    {
        return $this->hasMany(Peran::class, 'id');
    }
    
    public function Kritik()
    {
        return $this->hasMany(Kritik::class, 'id');
    }
}
