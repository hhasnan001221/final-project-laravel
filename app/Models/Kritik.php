<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kritik extends Model
{
    use HasFactory;

    protected $primaryKey = 'id';

    protected $fillable = ['nama', 'user_id', 'film_id', 'content', 'point'];

    public function Film()
	{
		return $this->belongsto(Film::class, 'id', 'id');
    }
   
    public function User()
	{
		return $this->belongsto(User::class, 'id', 'id');
    }
}
