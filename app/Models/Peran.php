<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Peran extends Model
{
    use HasFactory;

    protected $primaryKey = 'id_genre';

    protected $fillable = ['nama', 'film_id', 'casts_id'];

    public function Film()
    {
        return $this->belongsTo(Film::class, 'id', 'id');
    }

    public function Cast()
    {
        return $this->belongsTo(Cast::class, 'id', 'id');
    }
}
