<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    use HasFactory;
    
    protected $primaryKey = 'id';

    protected $fillable = ['nama'];

    public function Film()
	{
		return $this->hasMany(Film::class, 'id');
	}
}
