<?php

namespace App\Http\Controllers;

use App\Models\Peran;
use App\Models\Cast;
use App\Models\Film;
use Illuminate\Http\Request;

class PeranController extends Controller
{
    public function index()
	{
		$perans = Peran::with('film', 'cast')->get();

		return view('peran.index', compact('perans'));
	}

	public function tambah()
	{
		$films = Film::all();

		$casts = Cast::all();

		return view('peran.form', compact('films', 'casts'));
	}

	public function simpan(Request $request)
	{
		$data = [
			'nama' => $request->nama,
			'casts_id' => $request->casts_id,
			'film_id' => $request->film_id,
		];

		Peran::create($data);

		return redirect()->route('peran');
	}

	public function edit($id)
	{

		$perans = Peran::find($id);
		$films = Film::all();
		$casts = Cast::all();

		return view('peran.form', compact('perans', 'films', 'casts'));
	}

	public function update($id, Request $request)
	{
		$data = [
			'nama' => $request->nama,
			'film_id' => $request->film_id,
			'casts_id' => $request->casts_id,
		];

		Peran::find($id)->update($data);

		return redirect()->route('peran');
	}

	public function hapus($id)
	{
		Peran::find($id)->delete();

		return redirect()->route('peran');
	}
}
