<?php

namespace App\Http\Controllers;

use App\Models\Genre;
use Illuminate\Http\Request;

class GenreController extends Controller
{
    public function index()
	{
		$genres = Genre::all();

		return view('genre.index', compact('genres'));
	}

	public function tambah()
	{
		return view('genre.form');
	}

	public function simpan(Request $request)
	{
		$data = [
			'nama' => $request->nama,
		];

		Genre::create($data);

		return redirect()->route('genre');
	}

	public function edit($id)
	{
		$genres = Genre::find($id);

		return view('genre.form', compact('genres'));
	}

	public function update($id, Request $request)
	{
		$data = [
			'nama' => $request->nama,
		];

		Genre::find($id)->update($data);

		return redirect()->route('genre');
	}

	public function hapus($id)
	{
		Genre::find($id)->delete();

		return redirect()->route('genre');
	}

}
