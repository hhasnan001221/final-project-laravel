<?php

namespace App\Http\Controllers;

use App\Models\Film;
use App\Models\User;
use App\Models\Kritik;
use Illuminate\Http\Request;

class KritikController extends Controller
{
    public function index()
	{
		
		$kritiks = Kritik::all();

		return view('Kritik.index', compact('kritiks'));
	}

	public function tambah()
	{
		$users = User::all();
		$films = Film::all();
		return view('kritik.form', compact('users', 'films'));
	}

	public function simpan(Request $request)
	{
		$data = [
			'user_id' => $request->user_id,
			'film_id' => $request->film_id,
			'content' => $request->content,
			'point' => $request->point,
		];

		Kritik::create($data);

		return redirect()->route('kritik');
	}

	public function edit($id)
	{

		$kritiks = Kritik::find($id)->first();

		$users = User::all();

		$films = Film::all();

		return view('kritik.form', compact('kritiks', 'users', 'films'));
	}

	public function update($id, Request $request)
	{
		$data = [
			'user_id' => $request->user_id,
			'film_id' => $request->film_id,
			'content' => $request->content,
			'point' => $request->point,
		];

		Kritik::find($id)->update($data);

		return redirect()->route('kritik');
	}

	public function hapus($id)
	{
		Kritik::find($id)->delete();

		return redirect()->route('kritik');
	}
}
