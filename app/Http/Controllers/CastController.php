<?php

namespace App\Http\Controllers;

use App\Models\Cast;
use Illuminate\Http\Request;

class CastController extends Controller
{
	public function index()
	{
		$casts = Cast::all();

		return view('casts.index', compact('casts'));
	}

	public function tambah()
	{
		return view('casts.form');
	}

	public function simpan(Request $request)
	{
		$data = [
			'nama' => $request->nama,
			'umur' => $request->umur,
			'bio' => $request->bio,
		];

		Cast::create($data);

		return redirect()->route('casts');
	}

	public function edit($id)
	{

		$casts = Cast::find($id)->first();

		return view('casts.form', compact('casts'));
	}

	public function update($id, Request $request)
	{
		$data = [
			'nama' => $request->nama,
			'umur' => $request->umur,
			'bio' => $request->bio,
		];

		Cast::find($id)->update($data);

		return redirect()->route('casts');
	}

	public function hapus($id)
	{
		Cast::find($id)->delete();

		return redirect()->route('casts');
	}
}
