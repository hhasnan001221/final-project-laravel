<?php

namespace App\Http\Controllers;

use App\Models\Film;
use App\Models\Genre;
use Illuminate\Http\Request;

class FilmController extends Controller
{
    public function index()
	{
		$films = Film::with('genre')->get();

		return view('film.index', compact('films'));
	}

	public function tambah()
	{
		$genres = Genre::all();

		return view('film.form', compact('genres'));

	}

	public function simpan(Request $request)
	{
		$data = [
			'judul' => $request->judul,
			'ringkasan' => $request->ringkasan,
			'tahun' => $request->tahun,
			'poster' => $request->poster,
			'genre_id' => $request->genre_id,
		];

		Film::create($data);

		return redirect()->route('film');
	}

	public function edit($id)
	{
		$films = Film::find($id);
		$genres = Genre::all();

		return view('film.form', compact('genres', 'films'));
	}

	public function update($id, Request $request)
	{
		$data = [
			'judul' => $request->judul,
			'ringkasan' => $request->ringkasan,
			'tahun' => $request->tahun,
			'poster' => $request->poster,
			'genre_id' => $request->genre_id,
		];

		Film::find($id)->update($data);

		return redirect()->route('film');
	}

	public function hapus($id)
	{
		
		Film::find($id)->delete();

		return redirect()->route('film');
	}

	public function show($id)
    {
       $films = Film::with('genre')->get();

		return view('film.show', compact('films'));
    }

}
