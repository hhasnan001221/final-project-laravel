<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CastController;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\PeranController;
use App\Http\Controllers\TableController;
use App\Http\Controllers\KritikController;
use App\Http\Controllers\ProfilController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::controller(UserController::class)->group(function () {
    Route::get('register', 'register')->name('register');
	Route::post('register', 'registerSimpan')->name('register.simpan');
    
	Route::get('login', 'login')->name('login');
	Route::post('login', 'loginAksi')->name('login.aksi');
    
	Route::get('logout', 'logout')->middleware('auth')->name('logout');
});


Route::middleware('auth')->group(function () {
    
Route::get('/', function () {
    return view('master');
});
Route::controller(CastController::class)->prefix('casts')->group(function () {
    Route::get('', 'index')->name('casts');
    Route::get('tambah', 'tambah')->name('casts.tambah');
    Route::post('tambah', 'simpan')->name('casts.tambah.simpan');
    Route::get('edit/{id}', 'edit')->name('casts.edit');
    Route::post('edit/{id}', 'update')->name('casts.tambah.update');
    Route::get('hapus/{id}', 'hapus')->name('casts.hapus');
});


Route::controller(ProfilController::class)->prefix('profil')->group(function () {
    Route::get('', 'index')->name('profil');
    Route::get('tambah', 'tambah')->name('profil.tambah');
    Route::post('tambah', 'simpan')->name('profil.tambah.simpan');
    Route::get('edit/{id}', 'edit')->name('profil.edit');
    Route::post('edit/{id}', 'update')->name('profil.tambah.update');
    Route::get('hapus/{id}', 'hapus')->name('profil.hapus');
});
Route::controller(FilmController::class)->prefix('film')->group(function () {
    Route::get('', 'index')->name('film');
    Route::get('tambah', 'tambah')->name('film.tambah');
    Route::post('tambah', 'simpan')->name('film.tambah.simpan');
    Route::get('edit/{id}', 'edit')->name('film.edit');
    Route::post('edit/{id}', 'update')->name('film.tambah.update');
    Route::get('hapus/{id}', 'hapus')->name('film.hapus');
    Route::get('show/{id}', 'show')->name('film.show');
});
Route::controller(GenreController::class)->prefix('genre')->group(function () {
    Route::get('', 'index')->name('genre');
    Route::get('tambah', 'tambah')->name('genre.tambah');
    Route::post('tambah', 'simpan')->name('genre.tambah.simpan');
    Route::get('edit/{id}', 'edit')->name('genre.edit');
    Route::post('edit/{id}', 'update')->name('genre.tambah.update');
    Route::get('hapus/{id}', 'hapus')->name('genre.hapus');
});
Route::controller(KritikController::class)->prefix('kritik')->group(function () {
    Route::get('', 'index')->name('kritik');
    Route::get('tambah', 'tambah')->name('kritik.tambah');
    Route::post('tambah', 'simpan')->name('kritik.tambah.simpan');
    Route::get('edit/{id}', 'edit')->name('kritik.edit');
    Route::post('edit/{id}', 'update')->name('kritik.tambah.update');
    Route::get('hapus/{id}', 'hapus')->name('kritik.hapus');
});
Route::controller(PeranController::class)->prefix('peran')->group(function () {
    Route::get('', 'index')->name('peran');
    Route::get('tambah', 'tambah')->name('peran.tambah');
    Route::post('tambah', 'simpan')->name('peran.tambah.simpan');
    Route::get('edit/{id}', 'edit')->name('peran.edit');
    Route::post('edit/{id}', 'update')->name('peran.tambah.update');
    Route::get('hapus/{id}', 'hapus')->name('peran.hapus');
});

Route::get('/data-tables', [TableController::class,'index']);
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
