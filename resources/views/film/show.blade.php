@extends('layout.app')

@section('title', 'Film Page')


@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />

@section('contents')

<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Review-Data-film</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Show-Data-Film</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section>
            <div class="row">
                <div class="col-lg-12 col-12">
                    <div class="card">
                        <div class="card-header bg-dark">
                            <div class="text-center text-white">
                                Review Data Film
                            </div>
                        </div>
                        <div class="card-body">
                            <table class="table">
                                <tbody>
                                @foreach ($films as $row)
                                    <tr>
                                        <th>Judul</th>
                                        <td>:</td>
                                        <td>{{ $row->judul }}</td>
                                    </tr>
                                    <tr>
                                        <th>Ringkasan</th>
                                        <td>:</td>
                                        <td>{{ $row->ringkasan }}</td>
                                    </tr>
                                    <tr>
                                        <th>Tahun</th>
                                        <td>:</td>
                                        <td>{{ $row->tahun }}</td>
                                    </tr>
                                    <tr>
                                        <th>Poster</th>
                                        <td>:</td>
                                        <td>{{ $row->poster }}</td>
                                    </tr>
                                    <tr>
                                        <th>Genre</th>
                                        <td>:</td> 
                                        <td>{{ $row->genre->nama }} </td>
                                    </tr>
                                </tbody>
                                @endforeach
                            </table>
                            <br>
                            <button type="submit" class="btn btn-primary me-5" style="width: 40%"><a href="{{ route('film') }}" class="text-white">Kembali Halaman Data Film</a></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </section>


        </div>

        <footer class="main-footer">
            <div class="float-right d-none d-sm-block">
                <b>Version</b> 3.2.0
            </div>
            <strong>Copyright &copy; 2014-2021 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights
            reserved.
        </footer>

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <script src="{{ asset('admin320/plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('admin320/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('admin320/dist/js/adminlte.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('admin320/dist/js/demo.js') }}"></script>
</body>
@endsection

@section('js')
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function () {
            $('#tablefilm').DataTable();
        });
</script>
@endsection